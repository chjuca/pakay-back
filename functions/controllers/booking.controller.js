function createBooking(req, res) {

    return res.status(201).json({
      ok: true,
      message: "Booking Create",
    });
}

function getAllBooking(req, res) {

    return res.status(201).json({
      ok: true,
      message: "Booking Get",
    });
}


module.exports = {createBooking, getAllBooking};