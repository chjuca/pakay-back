function createRoom(req, res) {

    return res.status(201).json({
      ok: true,
      message: "Room Create",
    });
}

function getAllRoom(req, res) {

    return res.status(201).json({
      ok: true,
      message: "Room Get",
    });
}


module.exports = {createRoom, getAllRoom};