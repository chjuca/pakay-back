const functions = require("firebase-functions");
const {bookingRouting} = require("./route/booking.route");
const {roomRouting} = require("./route/room.route");

const bookingRoute = bookingRouting
const roomRoute = roomRouting

exports.booking = functions.https.onRequest(bookingRoute);
exports.room = functions.https.onRequest(roomRoute);
