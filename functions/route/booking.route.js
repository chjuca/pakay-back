const express = require("express");
const {createBooking, getAllBooking} = require("../controllers/booking.controller");


const bookingRouting = express();


bookingRouting.get("/", getAllBooking);
bookingRouting.post("/", createBooking);


module.exports = {bookingRouting};

