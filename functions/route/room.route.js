const express = require("express");
const {createRoom, getAllRoom} = require("../controllers/room.controller");

const roomRouting = express();

roomRouting.get("/", getAllRoom);
roomRouting.post("/", createRoom);


module.exports = {roomRouting};

